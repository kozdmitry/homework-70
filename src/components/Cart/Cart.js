import React from 'react';
import Button from "@material-ui/core/Button";

const Cart = ({cartItems, dishes, remove, add}) => {

    const totalCount = Object.keys(cartItems). reduce((sum, id) => sum + cartItems[id], 0);

    if (totalCount === 0) return <p>Корщина пуста</p>;

    let totalSum = 0;

    const items = Object.keys(cartItems).map(id => {
        const dish = dishes.find(d => d.id === id);
        const count = cartItems[id];

        if (count === 0) {
            return null;
        };

        const elementSum = count * dish.price;
        totalSum += elementSum;

        return (
            <>
            <p onClick={() => remove(id)}>{dish.name} ({dish.price} KGZ) x {cartItems[id]} = {elementSum}</p>
            </>
        )
    })
    return (
        <div>
            {items}
            <hr/>
            <p>Доставка: 150 KGS</p>
            <p>Общая сумма: {totalSum}</p>
            <Button onClick={add} variant="contained" color="secondary">Place order</Button>
        </div>
    );
};

export default Cart;