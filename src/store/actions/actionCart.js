import axios from "axios";

export const ADD_DISH ='ADD_DISH';
export const REMOVE_DISH ='REMOVE_DISH';

export const addDish = id => ({type: ADD_DISH, id});
export const removeDish = id => ({type: REMOVE_DISH, id});