import axios from "axios";

export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_FAILURE = 'FETCH_DISHES_FAILURE';

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, dishes});
export const fetchDishesFailure = () => ({type: FETCH_DISHES_FAILURE});

export const fetchDishes = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchDishesRequest());
            const response = await axios.get('https://cafe-homework70-default-rtdb.firebaseio.com/menu.json');

            const dishes = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            dispatch (fetchDishesSuccess(dishes));
        } catch (error) {
            dispatch (fetchDishesFailure(error));
        }
    };
};

export const postInfo = (input) => {
    return async dispatch => {
        try {
            await axios.post ('https://cafe-homework70-default-rtdb.firebaseio.com/orders.json', input);
        } catch (e) {
            console.log(e);
        }
    }
};
