import {FETCH_DISHES_FAILURE, FETCH_DISHES_REQUEST, FETCH_DISHES_SUCCESS} from "../actions/actionDishes";

const initialState = {
     loading: false,
     dishes: [],
     error: false
};


const dishesReducer = (state = initialState, action) => {
     switch (action.type) {
          case FETCH_DISHES_REQUEST:
               return {...state, loading: true, error: false};
          case FETCH_DISHES_SUCCESS:
               return {...state, loading: false, dishes: action.dishes};
          case FETCH_DISHES_FAILURE:
               return {...state, loading: false, error: true};
          default:
               return state;
     }
};

export default dishesReducer;