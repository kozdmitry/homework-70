import {ADD_DISH, REMOVE_DISH} from "../actions/actionCart";

const initialState = {
    cart: {}
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH:
            let newCount = 1;

            if(action.id in state.cart) {
                newCount = state.cart[action.id] + 1;
            }
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.id]: newCount
                }
            };
        case REMOVE_DISH:
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.id]: 0
                }
            };
        default:
            return state;
    }
};

export default cartReducer;