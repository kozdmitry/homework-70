import React, {useEffect, useState} from "react";
import {Container, CssBaseline, Dialog, DialogContent, DialogTitle, Grid, Paper, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes, postInfo} from "./store/actions/actionDishes";
import Dish from "../src/components/Dish/Dish";
import {addDish, removeDish} from "./store/actions/actionCart";
import Cart from "./components/Cart/Cart";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const App = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);
    const cartItems = useSelector(state => state.cart.cart);
    const [modalVisible, setVisibleModal] = useState(false);

    const [info, setInfo] = useState({
        name: '',
        address: '',
        phone: '',
    });

    const changeInfo = event => {
        const {name, value} = event.target;
        setInfo(prev =>({
            ...prev,
            [name]: value
        }));
    };

    useEffect(() => {
        dispatch(fetchDishes())
    }, [dispatch]);

    const add = id => {
        dispatch(addDish(id));
    };

    const remove = id => {
        dispatch(removeDish(id));
    };

    const modalOpen = () => {
        setVisibleModal(true);
    };

    const postInfoClient = (e) => {
        e.preventDefault();
        const allInfo = {
            info,
            cartItems
        }
        dispatch(postInfo(allInfo))
        setVisibleModal(false);
    };

    return (
        <>
            <CssBaseline/>
            <Container>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {dishes.map(dish =>(
                            <Dish
                                key={dish.id}
                                name={dish.name}
                                price={dish.price}
                                image={dish.image}
                                add={() => add(dish.id)}
                        />
                        ))}
                    </Grid>
                    <Grid item xs={6}>
                        <Paper p={5}>
                           <Cart
                           cartItems={cartItems}
                           dishes={dishes}
                           remove={remove}
                           add={() => modalOpen()}

                           />
                        </Paper>
                    </Grid>
                    <Dialog
                        open={modalVisible}
                        onClose={() => {setVisibleModal(false)}}
                    >
                        <DialogTitle>
                            <form onSubmit={postInfoClient}>
                            <DialogContent dividers>
                                Введите данные для заказа
                                <Typography gutterBottom>
                                    Ваш адрес:
                                    <TextField
                                        onChange={changeInfo}
                                        fullWidth
                                        name="address"
                                        spacing={3}
                                        variant="outlined"
                                        value={info.address}
                                    >
                                    </TextField>
                                </Typography>
                                <Typography gutterBottom>
                                    Имя:
                                    <TextField
                                        onChange={changeInfo}
                                        fullWidth
                                        name="name"
                                        spacing={3}
                                        variant="outlined"
                                        value={info.name}
                                    >
                                    </TextField>
                                </Typography>
                                <Typography>
                                    Телефон:
                                    <TextField
                                        onChange={changeInfo}
                                        fullWidth
                                        name="phone"
                                        spacing={3}
                                        variant="outlined"
                                        value={info.phone}
                                    >
                                    </TextField>
                                </Typography>
                            </DialogContent>
                            <Button type="submit" color="primary" variant="contained">
                                Create order
                            </Button>
                            </form>
                        </DialogTitle>

                    </Dialog>
                </Grid>
            </Container>
        </>
    )
};

export default App;
